#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <stdio.h>



int main() {
    printf("Testing started\n");
    run_tests();
    return 0;
}