#define _DEFAULT_SOURCE
#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

enum test_data{
    TEST_COUNT = 5
};

size_t h_capacity() {
    block_size capacity;
    capacity.bytes = REGION_MIN_SIZE;
    return capacity_from_size(capacity).bytes;
}

bool test_usual_success() {

    void *h = heap_init(h_capacity());
    if (h == NULL) return false;
    debug_heap(stdout, h);

    void *block = _malloc(256);
    if (block == NULL) return false;
    _free(block);

    munmap(h, REGION_MIN_SIZE);
    return true;
}

bool test_free_one_block() {
    void *h = heap_init(h_capacity());
    if (h == NULL) return false;
    void *b1 = _malloc(128);
    void *b2 = _malloc(64);
    if (b1 == NULL || b2 == NULL) return false;

    debug_heap(stdout, h);
    _free(b1);
    debug_heap(stdout, h);
    _free(b2);
    debug_heap(stdout, h);
    munmap(h, REGION_MIN_SIZE);
    return true;

}

bool test_free_two_blocks() {
    void *h = heap_init(h_capacity());
    if (h == NULL) return false;
    void *b1 = _malloc(64);
    void *b2 = _malloc(4);
    void *b3 = _malloc(128);

    if (b1 == NULL || b2 == NULL || b3 == NULL) return false;
    debug_heap(stdout, h);
    _free(b1);
    _free(b3);
    debug_heap(stdout, h);
    _free(b1);
    munmap(h, REGION_MIN_SIZE);
    return true;

}

bool test_memory_is_over_expansion() {
    void *h = heap_init(1);
    if (h == NULL) return false;
    void *b1 = _malloc(REGION_MIN_SIZE + 512);
    if (b1 == NULL) return false;
    debug_heap(stdout, h);

    struct block_header *block_h = h;
    size_t capacity = block_h->capacity.bytes;

    if (b1 != h + offsetof(struct block_header, contents) || capacity <= REGION_MIN_SIZE) return false;

    _free(b1);
    munmap(h, 2 * REGION_MIN_SIZE);
    return true;
}

bool test_memory_is_over_replace() {
    void *h = heap_init(h_capacity());
    if (h == NULL) return false;
    debug_heap(stdout, h);
    (void) mmap(h + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    void *b1 = _malloc(16384);
    if (b1 == NULL) return false;
    debug_heap(stdout, h);
    munmap(h, REGION_MIN_SIZE);
    return true;
}


typedef bool (*test)();

test tests[TEST_COUNT] = {&test_usual_success, &test_free_one_block, &test_free_two_blocks,
                          &test_memory_is_over_expansion, &test_memory_is_over_replace};

void run_tests() {
    for (int i = 0; i < 5; i++) {
        printf("______________________________________________\n");
        printf("Test %d:\n", i + 1);
        if (tests[i]()) printf("Test passed\n");
        else printf("Test failed\n");
    }
}
